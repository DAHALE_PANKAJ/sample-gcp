resource "google_compute_instance" "compute-apps1" {
   name          = "compute-apps1"
   machine_type  = "n1-standard-1"
   project    = "my-project-49407"
   zone          = "asia-east1-a" 
   boot_disk {
     initialize_params {
       image = "ubuntu-1604-lts"
     }
   }
network_interface {
     network = "default"
     access_config {
     }
   } 
   tags = ["apps1"]
 }
resource "google_compute_instance" "compute-apps2" {
   name          = "compute-apps2"
   machine_type  = "n1-standard-1"
   project       = "my-project-49407"
   zone          = "asia-east1-a"
   boot_disk {
     initialize_params {
       image = "ubuntu-1604-lts"
     }
   }
network_interface {
     network = "default"
     access_config {
     }
   }
   tags = ["apps2"]
 }

network_interface {
     network = "default"
     access_config {
     }
   }
   tags = ["apps2"]
 }
